#!/usr/bin/env bash
# Path to current script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TOOLS=$SCRIPT_DIR/$(cat toolspath); cd $TOOLS

# Add current working directory to executable namespace
export PATH=$PATH:$TOOLS
# Setup root directory for resolution of imports:
# the path of all local python libraries are relative to this
export PYTHONPATH=$TOOLS

# Each command outputs the run directory, which is then used
# by the next command to add parameters and generate information
# used by the simulator. For a list of available options for each
# command, run 'export PYTHONPATH=.; <command> --help'.
setup/meta.py \
  --log INFO \
  --script $SCRIPT_DIR/`basename "$0"` \
  --results-directory $SCRIPT_DIR/tmp \
  --seed 1 |
setup/dataset.py \
  --name cifar10 \
  --train-examples-per-class 5000 5000 5000 5000 5000 5000 5000 5000 5000 5000 \
  --validation-examples-per-class 0 0 0 0 0 0 0 0 0 0 |
setup/nodes/google-fl.py \
  --name 2-shards-eq-classes \
  --nb-nodes 1000 \
  --local-shards 2 \
  --shard-size 25 |
setup/topology/d_cliques/greedy_swap.py \
  --interclique smallworld \
  --max-clique-size 10 \
  --max-steps 1000 |
setup/model/gn_lenet.py |
simulate/algorithm/d_sgd.py \
  --batch-size 2 \
  --learning-momentum 0.9 \
  --clique-gradient \
  --learning-rate 0.002 |
simulate/logger.py \
  --accuracy-logging-interval 10\
  --nb-processes 1 |
simulate/run.py \
  --nb-epochs 100;
