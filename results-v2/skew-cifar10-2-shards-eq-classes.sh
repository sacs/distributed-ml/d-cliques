#!/usr/bin/env bash
# Path to current script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TOOLS=$SCRIPT_DIR/$(cat toolspath); cd $TOOLS

# Add current working directory to executable namespace
export PATH=$PATH:$TOOLS
# Setup root directory for resolution of imports:
# the path of all local python libraries are relative to this
export PYTHONPATH=$TOOLS

# Each command outputs the run directory, which is then used
# by the next command to add parameters and generate information
# used by the simulator. For a list of available options for each
# command, run 'export PYTHONPATH=.; <command> --help'.
for SEED in $(seq 100); do
setup/meta.py \
  --script $SCRIPT_DIR/`basename "$0"` \
  --results-directory $SCRIPT_DIR/skews-cifar10 \
  --seed $SEED |
setup/dataset.py \
  --name cifar10 \
  --train-examples-per-class 5000 5000 5000 5000 5000 5000 5000 5000 5000 5000 \
  --validation-examples-per-class 0 0 0 0 0 0 0 0 0 0 |
setup/nodes/google-fl.py \
  --name 2-shards-eq-classes \
  --nb-nodes 100 \
  --local-shards 2 \
  --shard-size 250 |
setup/topology/d_cliques/greedy_swap.py \
  --interclique fully-connected \
  --max-clique-size 10 \
  --max-steps 1000;
done
