#!/usr/bin/env bash
# Path to current script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TOOLS=$SCRIPT_DIR/../../../../../$(cat $SCRIPT_DIR/../../../../../toolspath); cd $TOOLS

# Add current working directory to executable namespace
export PATH=$PATH:$TOOLS
# Setup root directory for resolution of imports:
# the path of all local python libraries are relative to this
export PYTHONPATH=$TOOLS

# Each command outputs the run directory, which is then used
# by the next command to add parameters and generate information
# used by the simulator. For a list of available options for each
# command, run 'export PYTHONPATH=.; <command> --help'.
for MS in 5 10 20; do
    setup/meta.py \
      --results-directory $SCRIPT_DIR/all \
      --seed 1 |
    setup/dataset.py \
      --name cifar10 \
      --validation-examples-per-class 500 500 500 500 500 500 500 500 500 500 |
    setup/nodes/google-fl.py \
      --nb-nodes 100 \
      --local-shards 2 \
      --shard-size 225 |
    setup/topology/d_cliques/greedy_swap.py \
      --interclique ring \
      --max-clique-size $MS \
      --max-steps 1000 |
    setup/model/gn_lenet.py |
    simulate/algorithm/d_sgd.py \
      --batch-size 20 \
      --learning-momentum 0.9 \
      --learning-rate 0.002 \
      --clique-gradient |
    simulate/logger.py \
      --accuracy-logging-interval 10\
      --skip-testing\
      --nb-processes 4 |
    simulate/run.py \
      --nb-epochs 100;
done
