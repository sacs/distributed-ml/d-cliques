#!/usr/bin/env bash
# Path to current script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TOOLS=$SCRIPT_DIR/$(cat toolspath); cd $TOOLS

# Add current working directory to executable namespace
export PATH=$PATH:$TOOLS
# Setup root directory for resolution of imports:
# the path of all local python libraries are relative to this
export PYTHONPATH=$TOOLS

# Each command outputs the run directory, which is then used
# by the next command to add parameters and generate information
# used by the simulator. For a list of available options for each
# command, run 'export PYTHONPATH=.; <command> --help'.
for SEED in 1 2 3; do
setup/meta.py \
  --results-directory $SCRIPT_DIR/tmp \
  --seed $SEED |
setup/dataset.py \
  --name mnist \
  --train-examples-per-class 4943 5607 4926 5121 4860 4529 4960 5237 4877 4940 |
setup/nodes/google-fl.py \
  --name 2-shards-uneq-classes \
  --nb-nodes 1000 \
  --local-shards 2 \
  --shard-size 25 |
setup/topology/d_cliques/greedy_swap.py \
  --interclique ring \
  --max-clique-size 10 \
  --max-steps 1000 |
setup/model/linear.py |
simulate/algorithm/d_sgd.py \
  --batch-size 13 \
  --learning-momentum 0.0 \
  --learning-rate 0.1 \
  --clique-gradient |
simulate/logger.py \
  --nb-processes 8 |
simulate/run.py \
  --nb-epochs 100;
done
