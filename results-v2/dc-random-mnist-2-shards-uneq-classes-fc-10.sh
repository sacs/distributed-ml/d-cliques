#!/usr/bin/env bash
# Path to current script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TOOLS=$SCRIPT_DIR/$(cat toolspath); cd $TOOLS

# Add current working directory to executable namespace
export PATH=$PATH:$TOOLS
# Setup root directory for resolution of imports:
# the path of all local python libraries are relative to this
export PYTHONPATH=$TOOLS

# Each command outputs the run directory, which is then used
# by the next command to add parameters and generate information
# used by the simulator. For a list of available options for each
# command, run 'export PYTHONPATH=.; <command> --help'.
for C_GRAD in '--clique-gradient' ''; do
for SEED in 1 2 3; do
setup/meta.py \
  --script $SCRIPT_DIR/`basename "$0"` \
  --results-directory $SCRIPT_DIR/tmp \
  --seed $SEED |
setup/dataset.py \
  --name mnist \
  --train-examples-per-class 4900 5600 4900 5100 4800 4500 4900 5200 4800 4900 | 
setup/nodes/google-fl.py \
  --name 2-shards-uneq-classes \
  --nb-nodes 10 \
  --local-shards 2 \
  --shard-size 2480 |
setup/topology/d_cliques/random_cliques.py \
  --interclique fully-connected \
  --max-clique-size 10 |
setup/model/linear.py |
simulate/algorithm/d_sgd.py \
  --batch-size 1280 \
  --learning-momentum 0.0 \
  --learning-rate 0.1 \
  $C_GRAD |
simulate/logger.py \
  --nb-processes 8 |
simulate/run.py \
  --nb-epochs 100;
done;
done
