#!/usr/bin/env bash
# Path to current script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TOOLS=$SCRIPT_DIR/$(cat toolspath); cd $TOOLS

# Add current working directory to executable namespace
export PATH=$PATH:$TOOLS
# Setup root directory for resolution of imports:
# the path of all local python libraries are relative to this
export PYTHONPATH=$TOOLS

# Each command outputs the run directory, which is then used
# by the next command to add parameters and generate information
# used by the simulator. For a list of available options for each
# command, run 'export PYTHONPATH=.; <command> --help'.
for INIT_AVG in '--initial-averaging'; do
for U_GRAD in '--unbiased-gradient' ''; do
for SEED in 1; do
setup/meta.py \
  --script $SCRIPT_DIR/`basename "$0"` \
  --results-directory $SCRIPT_DIR/tmp \
  --seed $SEED |
setup/dataset.py \
  --name mnist \
  --train-examples-per-class 4500 4500 4500 4500 4500 4500 4500 4500 4500 4500 | 
setup/nodes.py \
  --name max-local-skew \
  --nb-nodes 100 \
  --local-classes 1 \
  --nodes-per-class 10 10 10 10 10 10 10 10 10 10 |
setup/topology/greedy_diverse_random.py \
  --nb-neighbours 10 |
setup/model/linear.py |
simulate/algorithm/d_sgd.py \
  --batch-size 128 \
  --learning-momentum 0.0 \
  $INIT_AVG \
  $U_GRAD \
  --learning-rate 0.1 |
simulate/logger.py \
  --nb-processes 8 |
simulate/run.py \
  --nb-epochs 100;
done;
done;
done
