#!/usr/bin/env bash
# Path to current script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TOOLS=$SCRIPT_DIR/../../../$(cat $SCRIPT_DIR/../../../toolspath); cd $TOOLS

# Add current working directory to executable namespace
export PATH=$PATH:$TOOLS
# Setup root directory for resolution of imports:
# the path of all local python libraries are relative to this
export PYTHONPATH=$TOOLS

for LR in 0.04 0.05; do
# Each command outputs the run directory, which is then used
# by the next command to add parameters and generate information
# used by the simulator. For a list of available options for each
# command, run 'export PYTHONPATH=.; <command> --help'.
setup/meta.py \
  --results-directory $SCRIPT_DIR/test \
  --seed 1 |
setup/dataset.py \
  --name mnist \
  --train-examples-per-class 4500 4500 4500 4500 4500 4500 4500 4500 4500 4500 | 
  #--global-train-ratios 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 |
setup/nodes.py \
  --nb-nodes 100 \
  --local-classes 10 \
  --nodes-per-class 100 100 100 100 100 100 100 100 100 100 |
setup/topology/ring.py \
  --metric random |
setup/model/linear.py |
simulate/algorithm/d_sgd.py \
  --batch-size 128 \
  --learning-momentum 0.0 \
  --learning-rate $LR |
simulate/logger.py \
  --accuracy-logging-interval 5 \
  --nb-processes 2 |
simulate/run.py \
  --nb-epochs 20;
done
