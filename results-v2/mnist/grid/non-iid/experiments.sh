#!/usr/bin/env bash
# Path to current script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TOOLS=$SCRIPT_DIR/../../../$(cat $SCRIPT_DIR/../../../toolspath); cd $TOOLS

# Add current working directory to executable namespace
export PATH=$PATH:$TOOLS
# Setup root directory for resolution of imports:
# the path of all local python libraries are relative to this
export PYTHONPATH=$TOOLS

# Each command outputs the run directory, which is then used
# by the next command to add parameters and generate information
# used by the simulator. For a list of available options for each
# command, run 'export PYTHONPATH=.; <command> --help'.
setup/meta.py \
  --results-directory $SCRIPT_DIR/all \
  --seed 1 |
setup/dataset.py \
  --name mnist \
  --global-train-ratios 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 |
setup/nodes.py \
  --nb-nodes 100 \
  --local-classes 1 \
  --nodes-per-class 10 10 10 10 10 10 10 10 10 10 |
setup/topology/grid.py \
  --metric random |
setup/model/linear.py |
simulate/algorithm/d_sgd.py \
  --batch-size 128 \
  --learning-momentum 0.0 \
  --learning-rate 0.1 |
simulate/logger.py |
simulate/run.py \
  --nb-epochs 100
