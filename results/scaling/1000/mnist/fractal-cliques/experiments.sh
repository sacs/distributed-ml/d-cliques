#!/usr/bin/env bash
TOOLS=../../../../../../learn-topology/tools; CWD="$(pwd)"; cd $TOOLS
BSZS='
    13
    '
LRS='
    0.1
    '
for BSZ in $BSZS; 
    do for LR in $LRS;
        do python sgp-mnist.py --nb-nodes 1000 --nb-epochs 100 --local-classes 1 --seed 1 --nodes-per-class 100 100 100 100 100 100 100 100 100 100 --global-train-ratios 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 --dist-optimization d-psgd --topology fractal-cliques --metric dissimilarity --learning-momentum 0. --sync-per-mini-batch 1 --results-directory $CWD/all --learning-rate $LR --batch-size $BSZ "$@" --parallel-training --nb-workers 10 --dataset mnist --model linear --clique-gradient --initial-averaging
    done;
done;

