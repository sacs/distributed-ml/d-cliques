#!/usr/bin/env bash
TOOLS=../../../../../learn-topology/tools; CWD="$(pwd)"; cd $TOOLS
BSZS='
    20
    '
LRS='
    0.002
    '
for BSZ in $BSZS; 
    do for LR in $LRS;
        do python sgp-mnist.py --nb-nodes 100 --nb-epochs 100 --local-classes 1 --seed 1 --nodes-per-class 10 10 10 10 10 10 10 10 10 10 --global-train-ratios 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 --dist-optimization d-psgd --topology greedy-diverse-10 --metric dissimilarity --learning-momentum 0.9 --sync-per-mini-batch 1 --results-directory $CWD/all --learning-rate $LR --batch-size $BSZ "$@" --single-process --nb-logging-processes 10 --dataset mnist --model gn-lenet --accuracy-logging-interval 1 --unbiased-gradient
    done;
done;

