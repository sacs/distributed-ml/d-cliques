#!/usr/bin/env bash
TOOLS=../../../../learn-topology/tools; CWD="$(pwd)"; cd $TOOLS
#    2560
#    1280
#    320
#    128
BSZS='
   12800
    '
# 7680
LRS='
    0.1
    '
for BSZ in $BSZS; 
    do for LR in $LRS;
        do python sgp-mnist.py --nb-nodes 1 --nb-epochs 100 --local-classes 10 --seed 1 --nodes-per-class 1 1 1 1 1 1 1 1 1 1 --global-train-ratios 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 0.802568 --dist-optimization d-psgd --topology fully_connected --metric dissimilarity --learning-momentum 0. --sync-per-mini-batch 1 --results-directory $CWD/all --learning-rate $LR --batch-size $BSZ "$@" --single-process
    done;
done;

