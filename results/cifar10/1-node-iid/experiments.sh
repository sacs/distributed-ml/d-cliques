#!/usr/bin/env bash
TOOLS=../../../../../Software/non-iid-topology-simulator/tools/v1; CWD="$(pwd)"; cd $TOOLS
BSZS='
    2000
    '
LRS='
    0.002
    '
for BSZ in $BSZS; 
    do for LR in $LRS;
        do python simulate.py --nb-nodes 1 --nb-epochs 100 --local-classes 10 --seed 1 --nodes-per-class 1 1 1 1 1 1 1 1 1 1 --global-train-ratios 1 1 1 1 1 1 1 1 1 1 --dist-optimization d-psgd --topology fully_connected --metric dissimilarity --learning-momentum 0.9 --sync-per-mini-batch 1 --results-directory $CWD/all --learning-rate $LR --batch-size $BSZ "$@" --single-process --dataset cifar10 --model gn-lenet --accuracy-logging-interval 10 --validation-set-ratio 0.5 --single-process --nb-logging-processes 1
    done;
done;

