#!/usr/bin/env bash
TOOLS=../../../../learn-topology/tools; CWD="$(pwd)"; cd $TOOLS
BSZS='
    20
    '
LRS='
    0.002
    '
for BSZ in $BSZS; 
    do for LR in $LRS;
        do python sgp-mnist.py --nb-nodes 100 --nb-epochs 100 --local-classes 1 --seed 1 --nodes-per-class 10 10 10 10 10 10 10 10 10 10 --global-train-ratios 1 1 1 1 1 1 1 1 1 1 --dist-optimization d-psgd --topology smallworld-logn-cliques  --metric dissimilarity --learning-momentum 0.9 --sync-per-mini-batch 1 --results-directory $CWD/all --learning-rate $LR --batch-size $BSZ "$@" --single-process --nb-logging-processes 10 --dataset cifar10 --model gn-lenet --clique-gradient --initial-averaging --accuracy-logging-interval 10 --validation-set-ratio 0.5
    done;
done;

