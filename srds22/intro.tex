% !TEX root = main.tex

\section{Introduction}

Machine learning is currently shifting from a \emph{centralized}
paradigm, where training data is located on a single
machine or
in a data center, to \emph{decentralized} ones in which data is processed
where it was naturally produced.
This shift is illustrated by the rise of Federated
Learning
(FL)~\cite{mcmahan2016communication}. FL allows
several parties (hospitals, companies, personal
devices...) to collaboratively train machine learning models
on their joint
data without centralizing it. Not only does FL
avoid the costs of moving data, but it also  mitigates privacy and
confidentiality concerns~\cite{kairouz2019advances}.
Yet, working with natural data distributions introduces new challenges for
learning systems, as
local datasets
reflect the usage and production patterns specific to each participant: in
other words, they are
\emph{heterogeneous}. An important type of data heterogeneity encountered in
federated classification problems, known as \emph{label distribution skew} 
\cite{kairouz2019advances,quagmire}, occurs when the frequency of different
classes of examples varies significantly across local datasets.
A key challenge in FL is to design algorithms that
can efficiently deal with such heterogeneous data distributions
\cite{kairouz2019advances,fedprox,scaffold,quagmire}.

Federated learning algorithms can be classified into two categories depending
on the underlying network topology they run on. In server-based FL, the
network is organized according to a star topology: a central server orchestrates the training process by
iteratively aggregating model updates received from the participants
(\emph{clients}) and sending back the aggregated model \cite{mcmahan2016communication}. In contrast,
fully decentralized FL algorithms operate over an arbitrary network topology
where participants communicate only with their direct neighbors
in the network. A classic example of such algorithms is Decentralized
SGD (D-SGD) \cite{lian2017d-psgd}, in which participants alternate between
local SGD updates and model averaging with neighboring nodes.

In this paper, we focus on fully decentralized algorithms as they can
generally scale better to the large number of participants seen in ``cross-device''
applications \cite{kairouz2019advances}. Effectively, while a central
server may quickly become a bottleneck as the number of participants increases, the topology used in fully decentralized algorithms can remain sparse
enough such that all participants need only to communicate with a small number of other participants, i.e. nodes have small (constant or logarithmic) degree 
\cite{lian2017d-psgd}. In the homogeneous setting where data is
independent and identically distributed (IID) across nodes, recent work
has shown both empirically
\cite{lian2017d-psgd,Lian2018} and theoretically \cite{neglia2020} that sparse
topologies like rings or grids
do not significantly affect the convergence
speed compared to using denser topologies.

% From directory 'results-v2':
% MNIST
% python $TOOLS/analyze/filter.py all --dataset:name mnist --topology:name ring grid fully-connected --nodes:name iid-uneq-classes 2-shards-uneq-classes --meta:seed 1 --nodes:nb-nodes 100 | python $TOOLS/analyze/diff.py

% Ring
% python $TOOLS/analyze/diff.py --rundirs all/2021-09-28-18:41:09-CEST-labostrex112 all/2021-09-28-23:11:59-CEST-labostrex113  --pass-through | python $TOOLS/plot/convergence.py --add-min-max --ymin 0 --ymax 92.5 --yaxis test-accuracy --labels '100 nodes homogeneous' '100 nodes skewed' --save-figure ../mlsys2022style/figures/ring-IID-vs-non-IID-uneq-classes.png --linestyles 'solid' 'dashed' --font-size 18 --linewidth 3
% Grid
% python $TOOLS/analyze/diff.py --rundirs all/2021-09-28-18:44:32-CEST-labostrex115 all/2021-09-28-23:14:36-CEST-labostrex114  --pass-through | python $TOOLS/plot/convergence.py --add-min-max --ymin 0 --ymax 92.5 --yaxis test-accuracy --labels '100 nodes homogeneous' '100 nodes skewed' --save-figure ../mlsys2022style/figures/grid-IID-vs-non-IID-uneq-classes.png --linestyles 'solid' 'dashed' --font-size 18 --linewidth 3
% Fully-Connected
% python $TOOLS/analyze/diff.py --rundirs all/2021-09-28-18:46:48-CEST-labostrex116 all/2021-09-28-23:16:47-CEST-labostrex117   --pass-through | python $TOOLS/plot/convergence.py --add-min-max --ymin 0 --ymax 92.5 --yaxis test-accuracy --labels '100 nodes homogeneous' '100 nodes skewed' --save-figure ../mlsys2022style/figures/fc-IID-vs-non-IID-uneq-classes.png --linestyles 'solid' 'dashed' --font-size 18 --linewidth 3

\begin{figure*}[t]
     \centering
     \begin{subfigure}[b]{0.25\textwidth}
         \centering
         \includegraphics[width=\textwidth]{figures/ring-IID-vs-non-IID-uneq-classes}
\caption{\label{fig:ring-IID-vs-non-IID-uneq-classes} Ring topology}
     \end{subfigure}
     \quad
     \begin{subfigure}[b]{0.25\textwidth}
         \centering
         \includegraphics[width=\textwidth]{figures/grid-IID-vs-non-IID-uneq-classes}
\caption{\label{fig:grid-IID-vs-non-IID-uneq-classes} Grid topology}
     \end{subfigure}
     \quad
     \begin{subfigure}[b]{0.25\textwidth}
         \centering
         \includegraphics[width=\textwidth]{figures/fc-IID-vs-non-IID-uneq-classes}
\caption{\label{fig:fully-connected-IID-vs-non-IID-uneq-classes} Fully-connected topology}
     \end{subfigure}
        \caption{Convergence speed of decentralized
        SGD with and without label distribution skew for different topologies.
        The task is logistic regression on MNIST (see
        Section~\ref{section:experimental-settings} for details on
        the experimental setup). Bold lines show the
        average test
        accuracy across nodes
        while thin lines show the minimum
        and maximum accuracy of individual nodes. While the effect of topology
        is negligible for homogeneous data, it is very significant in the
        heterogeneous case. On a fully-connected network, both cases converge
        similarly.}
        \label{fig:iid-vs-non-iid-problem}
\end{figure*}

In contrast to the homogeneous case, our experiments demonstrate that 
\emph{the impact of topology is extremely significant under heterogeneous
data}.
This phenomenon is illustrated in Figure~\ref{fig:iid-vs-non-iid-problem}: we observe that under
label distribution skew, using a
sparse topology clearly jeopardizes the convergence speed of decentralized
SGD.\footnote{Unlike in centralized FL
\cite{mcmahan2016communication,scaffold,quagmire}, this
happens even when nodes perform a single local update before averaging the
model with their neighbors.} This strong impact of
data heterogeneity and its possible interplay with the topology is not well
captured by current theoretical
analyses of
decentralized FL, as they model heterogeneity by some (unknown)
constant that bounds the variance of local gradients, independently of the
topology \cite{lian2017d-psgd,Lian2018,neglia2020,ying2021exponential}.
Motivated by the above empirical observations, in this paper we address the
following question:

\textit{Can we design sparse topologies with  convergence
  speed similar to a  fully connected network for problems involving
  many participants with label distribution skew?}
  
% Moreover, common analysis techniques to compare the asymptotic convergence behavior
% of different topologies assume the impact on gradients is bounded by an unknown constant  (e.g.~\cite{ying2021exponential}), and therefore ignore it. For example, asymptotic analysis suggests that an expander graph, a sparse topology whose number of edges per node scales logarithmically with the number of nodes,  achieves exact averaging~\cite{ying2021exponential}: if this were the case, this would remove the effect of the topology on distributed averaging and reduce D-SGD to FedSGD~\cite{mcmahan2016communication} regardless of how data is partitioned. However, in practice as we will show, not only is the effect of data heterogeneity sufficient to prevent exact averaging, a different topology using as many or less edges can converge faster. In the rest of this paper we therefore rely on rigorous, repeatable practical experiments, instead of asymptotic convergence analysis, to accurately compare the convergence speed of a wide diversity of topologies. This provides a stronger basis for quantifying the effect of data heterogeneity and should motivate future work in adapting analysis techniques to correctly include it. 

Specifically, we make the following contributions:
(1) We propose D-Cliques, a sparse topology in which nodes are organized in
interconnected cliques (i.e., locally fully-connected sets of nodes) such that
the joint label distribution of each clique is close to that of the global 
distribution; (2) We design Greedy Swap, a randomized greedy algorithm for
constructing such cliques efficiently;
% in the presence of heterogeneity previously studied
% in the context of Federated Learning~\cite{mcmahan2016communication};
 (3) We introduce Clique Averaging, a modified version of 
the standard D-SGD algorithm:  Clique Averaging decouples gradient averaging, used for
optimizing local models, from distributed averaging, used to ensure that all
models converge, thereby reducing the bias introduced by inter-clique
connections; 
(4) We show how Clique Averaging can be used to implement unbiased momentum
that would otherwise be detrimental in the heterogeneous setting; (5) Through
an extensive experimental study on decentralized learning of linear
models and deep
convolutional networks on MNIST %~\cite{mnistWebsite}
and CIFAR10 datasets, % ~\cite{krizhevsky2009learning}
we validate our various design choices and
demonstrate that our approach is able to remove the effect
of label distribution skew while maintaining a sparse topology;
(6) Finally, we demonstrate the scalability of our
approach by considering  up to 1000-node networks, in contrast to most
previous work on fully decentralized learning which performs empirical
evaluations on networks with
at most a few tens
of nodes
\cite{tang18a,neglia2020,momentum_noniid,cross_gradient,consensus_distance}.

For instance, our results show that under strong label distribution skew,
using D-Cliques in a 1000-node network
requires 98\% less edges ($18.9$ vs $999$ edges per participant on average) to obtain a similar convergence speed as a fully-connected topology,
thereby yielding a 96\% reduction in the total number of required messages 
(37.8 messages per round per node on average instead of 999). An additional 22\% improvement
% (14.5 edges per node on average instead of 18.9)
is possible when using a small-world inter-clique topology, with further
potential gains at larger scales through a quasilinear $O(n
\log n)$ scaling in the number of nodes $n$.
We also show that D-Cliques empirically provide faster and more
robust convergence than other topologies with a similar number of edges, such
as random graphs and the exponential graphs recently promoted in 
\cite{ying2021exponential}.

The rest of this paper is organized as follows.
We first describe the problem setting in Section~\ref{section:problem}. We
then present the design of D-Cliques in Section~\ref{section:d-cliques}.
Section~\ref{section:evaluation}
compares D-Cliques to different topologies 
and algorithmic variations to demonstrate their benefits, constructed with and without Greedy Swap
in an extensive experimental study. Finally, we review some related work
in Section~\ref{section:related-work}, and conclude with promising directions
for future work in Section~\ref{section:conclusion}.