% !TEX root = main.tex

\section{D-Cliques}
\label{section:d-cliques}

In this section, we introduce D-Cliques, a topology
designed to compensate for data heterogeneity. We also present some
modifications of D-SGD that leverage some properties of the proposed
topology and enable the implementation of a successful momentum scheme.

\subsection{Intuition}

To give the intuition behind
our approach, let us consider the neighborhood of a single node in a grid
topology represented
on Figure~\ref{fig:grid-iid-vs-non-iid-neighbourhood}.
% in which each node has examples
% of a single class.
Nodes are distributed randomly in the grid and the colors of a node represent
the proportion of each class in its local dataset. In the homogeneous
setting, the label distribution is the same across
nodes: in the example shown in Figure~\ref{fig:grid-iid-neighbourhood}, all classes
are represented in equal proportions on all nodes. This is not the case in the
heterogeneous setting: Figure~\ref{fig:grid-non-iid-neighbourhood} shows an
extreme case of label distribution skew where each
node holds examples of a single class only.

From the point of view of the center node in
Figure~\ref{fig:grid-iid-vs-non-iid-neighbourhood}, a single training step of
D-SGD is
equivalent to sampling a mini-batch five times larger from the union of the
local distributions of neighboring nodes.
In the homogeneous case, since gradients are computed from examples of all
classes,
the resulting averaged gradient  points in a direction that tends to reduce
the loss across all classes. In contrast, in the heterogeneous case, the
representation of classes in the immediate neighborhood of the node is
different from the global label distribution
(in Figure~\ref{fig:grid-non-iid-neighbourhood}, only a
subset of classes are represented), thus the gradients will
be biased.
Importantly, as the distributed averaging process takes several steps to
converge, this variance persists across iterations as the locally computed
gradients are far from the global average.\footnote{One could perform a
sufficiently large number of
averaging steps between each gradient step, but this is too costly in
practice.} This can significantly slow down
convergence speed to the point of making decentralized optimization
impractical. 

\begin{figure}[t]
     \centering
     \begin{subfigure}[b]{0.16\textwidth}
         \centering
         \includegraphics[width=\textwidth]{figures/grid-iid-neighbourhood}
\caption{\label{fig:grid-iid-neighbourhood} Homogeneous data}
     \end{subfigure}
     \hspace*{.5cm}
     \begin{subfigure}[b]{0.16\textwidth}
         \centering
         \includegraphics[width=\textwidth]{figures/grid-non-iid-neighbourhood}
\caption{\label{fig:grid-non-iid-neighbourhood}  Heterogeneous data}
     \end{subfigure}
        \caption{Neighborhood in a grid.}
        \label{fig:grid-iid-vs-non-iid-neighbourhood}
\end{figure}

With D-Cliques, we address label distribution skew by
carefully designing a
network topology composed of \textit{locally representative cliques} while 
maintaining \textit{sparse inter-clique connections} only.
% The observation we make here has inspired refined theoretical analyses
% that now consider the effect of the neighbourhood data distribution on the averaged gradient
% and can formally explain the benefits of D-Cliques~\cite{bars2022yes,dandi2022mixing}.

\subsection{Constructing Locally Representative Cliques}

\begin{figure}[t]
  \hrule
   \begin{algorithmic}[1]
        \State \textbf{Require:} maximum clique size $M$, max steps $K$, set
        of all nodes $N = \{ 1, 2, \dots, n \}$,
        % \State $\textit{skew}(S)$: skew of subset $S \subseteq N$ compared to the global distribution (Eq.~\ref{eq:skew}), 
        procedure $\texttt{inter}(\cdot)$ to create inter-clique connections
        (see Sec.~\ref{section:interclique-topologies}) % \State $
        % \textit{weights}(E)$: set weights to edges in $E$ (Eq.~\ref{eq:metro}).
         \State $DC \leftarrow []$ %\COMMENT{Empty list}
         \While {$N \neq \emptyset$}
         \State $C \leftarrow$ sample $M$ nodes from $N$ at random
         \State $N \leftarrow N \setminus C$; $DC.\text{append}(C)$
         \EndWhile
         \For{$k \in \{1, \dots, K\}$}
        \State $C_1,C_2 \leftarrow$ random sample of 2 elements from $DC$
          \State $s \leftarrow \textit{skew}(C_1) + skew(C_2)$
        \State $\textit{swaps} \leftarrow []$
        \For{$i \in C_1, j \in C_2$}
          \State $s' \leftarrow \textit{skew}(C_1\setminus\{i\}\cup\{j\})
          + \textit{skew}(C_2 \setminus\{j\}\cup\{i\})$\hspace*{-.05cm}
          \If {$s' < s$}
            \State \textit{swaps}.append($(i, j)$)
          \EndIf
        \EndFor
        \If {len(\textit{swaps}) $> 0$}
          \State $(i,j) \leftarrow$ random element from $
          \textit{swaps}$ 
          \State $C_1 \leftarrow C_1 \setminus\{i\}\cup\{j\}; C_2 \leftarrow C_2 \setminus\{j\}\cup\{i\}$
        \EndIf
         \EndFor
        \State $E\leftarrow \{(i,j) : C\in DC, i,j\in C, i\neq j\}$
         % \State $G \leftarrow$ graph composed of the cliques in $DC$
        \Return topology $G=(N,E \cup 
        \texttt{inter}(DC))$
   \end{algorithmic}
   \hrule
    \caption{Greedy-Swap algorithm for D-Cliques construction.}
   \label{Algorithm:greedy-swap}
\end{figure}

D-Cliques construct a topology in which each node is part of a \emph{clique} 
(i.e., a subset of nodes whose induced subgraph is fully connected)
such that the label distribution in each clique is
close to the global label distribution. Formally, for a label $y$ and a
clique composed of nodes $C\subseteq N$, we denote by $p_C(y)=
\frac{1}{|C|}\sum_{i\in C} p_i(y)$ the distribution of $y$ in $C$
and by $p(y)=\frac{1}{n}\sum_{i\in N} p_i(y)$ its global distribution.
We measure the \textit{skew} of $C$ by the L1 distance, i.e. the sum
of the absolute differences of $p_C(y)$ and $p(y)$ over $y$, which is a
commonly used
measure of distance between histograms:
\begin{equation}
\label{eq:skew}
        \textit{skew}(C) =\
    \sum_{l=1}^L | p_C(y = l) - p(y = l) |.
\end{equation}


% D-Cliques constructs a topology in which each node $i \in N$ is
% part of a \textit{clique} $C$ such that the clique distribution $D_C =
% \bigcup\limits_{\substack{i \in C}} D_i$ is close to that of the global
% distribution $D = \bigcup\limits_{\substack{i \in N}} D_i$. We measure the
% closeness of $D_C$ to $D$ using its \textit{skew}, i.e. the sum of the
% differences in the probabilities that a sample $(x,y)$ belongs to a given class in $D_C$ and $D$: 
% \begin{equation}
% \label{eq:skew}
% \begin{split}
%         \textit{skew}(C) =\
%     \sum\limits_{\substack{l=1}}^L | p(y = l~|(x,y) \sim D_C) - \\ p(y = l~|
%     (x,y) \sim D) |
% \end{split}
% \end{equation}

To efficiently construct a set of cliques with small skew, we propose
Greedy-Swap (Figure~\ref{Algorithm:greedy-swap}). The parameter
$M$ is the maximum size of cliques and controls the
number of intra-clique edges. We start by initializing cliques at
random. Then, for
a certain number of steps $K$, we randomly pick two cliques and swap two of
their nodes so as to decrease the sum of skews of the two cliques. The swap is
chosen randomly among the ones that decrease the skew, hence
this algorithm can be seen as a form of randomized greedy algorithm.

In practice, Greedy-Swap may be executed by a single party (which could be one
of the nodes): in this case, each node $i$ should send
to this party its label distribution $p_i(y)$ at each node $i$. 
Alternatively, Greedy-Swap may be executed in a decentralized fashion, e.g. with a
leader-based implementation, as follows. We assume that each node has a unique,
randomly assigned identifier. As a pre-processing step, the global label
distribution $p
(y)$ is computed by running a standard decentralized averaging
algorithm (e.g. \cite{kempe2003gossip}) so that all nodes know $p
(y)$. Cliques are initialized using the random identifiers and in each
clique, the node with smallest identifier becomes the clique leader. Leaders
from each clique then randomly sample other leaders with
peer sampling~\cite{jelasity2007gossip} to find potential swap candidates.
Once a leader $l_1$ has found another leader $l_2$, $l_1$ sends the label
distributions of all its individual clique members to $l_2$, who compares the
distributions to its own and picks a pair of nodes to swap such that the sum
of the skew of both cliques will decrease. After a swap, the leaders of each
clique update their clique members with the new memberships (including the
leaders if needed). Swapping continues for a predefined number of steps or
until the topology stabilizes.

% An evaluation of a decentralized implementation is outside 
% the scope of this paper: our goal in the rest of the paper is instead to show the 
% resulting D-Cliques topology to provide similar convergence speed as a 
% fully-connected topology so these results may serve as a foundation to
% design efficient and resilient decentralized topology optimizers for a variety of settings.


% \begin{algorithm}[h]
%    \caption{D-Cliques Construction: Greedy Swap}
%    \label{Algorithm:D-Clique-Construction}
%    \begin{algorithmic}[1]
%         \State \textbf{Require:} Max clique size $M$, Max steps $K$,
%         \State Set of all nodes $N = \{ 1, 2, \dots, n \}$,
%         \State $\textit{skew}(S)$: skew of subset $S \subseteq N$ compared to the global distribution (Eq.~\ref{eq:skew}), 
%         \State $\textit{intra}(DC)$: edges within cliques $C \in DC$,
%         \State $\textit{inter}(DC)$: edges between $C_1,C_2 \in DC$ (Sec.~\ref{section:interclique-topologies}),
%          \State $\textit{weights}(E)$: set weights to edges in $E$ (Eq.~\ref{eq:metro}).
%          \State ~~
%          \State $DC \leftarrow []$ \COMMENT{Empty list}
%          \WHILE {$N \neq \emptyset$}
%          \State $C \leftarrow$ sample $M$ nodes from $N$ at random
%          \State $N \leftarrow N \setminus C$; $DC.append(C)$
%          \ENDWHILE
%          \FOR{$k \in \{1, \dots, K\}$}
%         \State $C_1,C_2 \leftarrow$ sample 2 from $DC$ at random
%         \State $\textit{swaps} \leftarrow []$
%         \FOR{$n_1 \in C_1, n_2 \in C_2$}
%           \State $s \leftarrow skew(C_1) + skew(C_2)$
%           \State $s' \leftarrow \textit{skew}(C_1-n_1+n_2) + \textit{skew}(C_2 -n_2+n_1)$
%           \IF {$s' < s$}
%             \State \textit{swaps}.append($(n_1, n_2)$)
%           \ENDIF
%         \ENDFOR
%         \IF {\#\textit{swaps} $> 0$}
%           \State $(n_1,n_2) \leftarrow$ sample 1 from $\textit{swaps}$ at random
%           \State $C_1 \leftarrow C_1 - n_1 + n_2; C_2 \leftarrow C_2 - n_2 + n1$
%         \ENDIF
%          \ENDFOR
%         \RETURN $(weights(\textit{intra}(DC) \cup \textit{inter}(DC)), DC)$
%    \end{algorithmic}
% \end{algorithm}
\begin{remark}[Clique Size]
In practice, the clique size is chosen such that each class can be represented
in each clique, which can be inferred from the label distributions across
nodes, but no bigger than the maximum communication budget desired for each
node, which depends on the deployment environment and system constraints. In
general, the larger the clique size, the faster the convergence (in number of
rounds), but the higher the communication costs per round. The best clique
size can be tuned empirically based on the trade-off between communication
cost and clique skew.
\end{remark}

\begin{remark}[Privacy]
Constructing cliques with Greedy-Swap only requires nodes to reveal their
local label proportions (i.e., a histogram with $L$ bins), which is compact
and aggregated information. To further reduce the leakage in applications 
where the label membership of a data point may be sensitive information, techniques
from differential privacy \cite{dwork2013Algorithmic} can be readily used. 
%used to hide the influence of any individual data point on the histogram by adding a small amount of random noise to its entries before sharing it.
In particular, given the desired privacy level $\epsilon>0$ (the smaller, the
more privacy), each node $i$ can
share a \textit{private histogram} by adding independent
centered Laplace noise with standard deviation $\sigma=1/(m_i\epsilon)$ to
each
entry of its true histogram, where $m_i$
is the number of examples at node $i$. This gives an unbiased estimate of the
true histogram with a variance that increases
with the desired privacy level. Thus, the skew of cliques, and thereby the
resulting convergence speed of D-SGD, will be inversely proportional to the
privacy level. In the limit of $\epsilon\rightarrow 0$ (perfect privacy), the
allocation of nodes in cliques will become fully random. However, for a
reasonable privacy budget (say $\epsilon=1$, which is generally considered as
a good guarantee) and $m_i\simeq 500$ as used for all experiments with 100
nodes in Section~\ref{section:evaluation}, the impact of privacy will be negligible as~$\sigma\simeq 0.002$.
\end{remark}



The key idea of D-Cliques, implemented by the above Greedy-Swap algorithm, is
to ensure the clique-level label distribution
$p_C(y)$
 matches closely the global distribution $p(y)$. As a consequence,
the local models of nodes across cliques will remain rather close throughout
the iterations of D-SGD. Therefore, a
sparse inter-clique topology can be used, significantly reducing the total
number of edges without slowing down the convergence. We discuss some possible
choices for this inter-clique topology in the following section.

\subsection{Adding Sparse Inter-Clique Connections}
\label{section:interclique-topologies}

To ensure a global consensus and convergence, we introduce
\textit{inter-clique connections} between a small number of node pairs that
belong to different cliques, thereby implementing the \texttt{inter}
procedure called at the end of \autoref{Algorithm:greedy-swap}.
We aim to ensure that the degree of each node remains low and balanced so as
to make the network topology well-suited to decentralized federated learning.
We consider several choices of inter-clique topology, which offer
different scalings for the number of required edges and the average distance
between nodes in the resulting graph.

The \textit{ring} has (almost) the fewest possible number of edges for the
graph to be connected: in this case, each clique is connected to exactly
two other cliques by a single edge. This topology requires only $O(\frac{n}
{M})$ inter-clique edges but suffers an $O(n)$ average distance between nodes.

The
\textit{fractal} topology
provides a logarithmic bound on the average distance. In this
hierarchical scheme, cliques are arranged in larger groups of $M$ cliques that
are connected
internally with one edge per
pair of cliques, but with only one edge between pairs of larger groups. The
topology is built recursively such that $M$ groups will themselves form a
larger group at the next level up. This results in at most $M$ edges per node 
if edges are evenly distributed: i.e., each group within the same level adds 
at most $M-1$ edges to other groups, leaving one node per group with $M-1$ 
edges that can receive an additional edge to connect with other groups at the next level.
Since nodes have at most $M$ edges, the total number of inter-clique edges
is at most $nM$ edges.

We can also design an inter-clique topology in which the number of edges
scales in a log-linear fashion by following a
small-world-like topology~\cite{watts2000small} applied on top of a
ring~\cite{stoica2003chord}. In this scheme, cliques are first arranged in a
ring. Then each clique adds symmetric edges, both clockwise and
counter-clockwise on the ring, with the $c$ closest cliques in sets of
cliques that are exponentially bigger the further they are on the ring (see extended paper~\cite{dcliques-arxiv} for
details on the construction). This topology ensures a good connectivity with
other cliques that are close on the ring, while keeping the average
distance small. This scheme uses $O(c\frac{n}{M}\log\frac{n}{M})$ edges,
i.e.
log-linear in $n$.

\begin{figure}[t]
    \centering
    \includegraphics[width=0.18\textwidth]{figures/fully-connected-cliques}
    \caption{\label{fig:d-cliques-figure} D-Cliques with $n=100$, $M=10$ and a
fully connected inter-clique topology on a problem with 1 class/node.}
\end{figure}

Finally, we can consider a \emph{fully connected} inter-clique topology
 such that each clique has exactly
one edge with each of the other cliques, spreading these additional edges
equally among the nodes of a clique, as illustrated in Figure~\ref{fig:d-cliques-figure}. 
This has the advantage of
bounding the distance between any pair of nodes to $3$ but requires
$O(\frac{n^2}{M^2})$ inter-clique edges, i.e. quadratic in $n$.


%\todo{AB: if time, could add fig of another inter-clique topology (ring,
%fractal or small-world)}

%  In the following, we introduce
%  up to one
%  inter-clique
%  connection per node such that each clique has exactly one
% edge with all other cliques, see Figure~\ref{fig:d-cliques-figure} for the
% corresponding D-Cliques network in the case of $n=100$ nodes and $L=10$
% classes. We will explore sparser inter-clique topologies in
% Section~\ref{section:interclique-topologies}.

% So far, we have used a fully-connected inter-clique topology for D-Cliques,
% which has the advantage of bounding the
% \textit{path length}\footnote{The \textit{path length} is the number of edges on the path with the shortest number of edges between two nodes.} to $3$ between any pair of nodes. This choice requires $
% \frac{n}{c}(\frac{n}{c} - 1)$ inter-clique edges, which scales quadratically
% in the number of nodes $n$ for a given clique size $c$\footnote{We consider \textit{directed} edges in the analysis: the number of undirected edges is half and does not affect asymptotic behavior.}. This can become significant at larger scales when $n$ is
% large compared to $c$.

% We first measure the convergence speed of inter-cliques topologies whose number of edges scales linearly with the number of nodes. Among those, the \textit{ring} has the (almost) fewest possible number of edges: it
% uses $\frac{2n}{c}$ inter-clique edges but its average path length between nodes 
% also scales linearly.
% We also consider another topology, which we call \textit{fractal}, that provides a
% logarithmic
% bound on the average path length. In this hierarchical scheme, 
% cliques are assembled in larger groups of $c$ cliques that are connected internally with one edge per
% pair of cliques, but with only one edge between pairs of larger groups. The
% topology is built recursively such that $c$ groups will themselves form a
% larger group at the next level up. This results in at most $c$ edges per node 
% if edges are evenly distributed: i.e., each group within the same level adds 
% at most $c-1$ edges to other groups, leaving one node per group with $c-1$ 
% edges that can receive an additional edge to connect with other groups at the next level.
% Since nodes have at most $c$ edges, $n$ nodes have at most $nc$ edges, therefore
% the number of edges in this fractal scheme indeed scales linearly in the number of nodes.

% Second, we look at another scheme 
% in which the number of edges scales in a near, but not quite, linear fashion.
% We propose to connect cliques according to a
% small-world-like topology~\cite{watts2000small} applied on top of a
% ring~\cite{stoica2003chord}. In this scheme, cliques are first arranged in a
% ring. Then each clique adds symmetric edges, both clockwise and
% counter-clockwise on the ring, with the $m$ closest cliques in sets of
% cliques that are exponentially bigger the further they are on the ring (see
% Algorithm~\ref{Algorithm:Smallworld} in the appendix for
% details on the construction). This ensures a good connectivity with other
% cliques that are close on the ring, while still keeping the average
% path length small. This scheme uses $\frac{n}{c}*2(m)\log(\frac{n}{c})$ inter-clique edges and
% therefore grows in the order of $O(n\log(n))$ with the number of nodes.

\subsection{Optimizing over D-Cliques with Clique Averaging and Momentum}
\label{section:clique-averaging-momentum}

% In this section, we present Clique Averaging. This feature, when added to D-SGD,
% removes the bias caused by the inter-cliques edges of
% D-Cliques. We also show how it can be used to successfully implement momentum
% for non-IID data.

% \subsubsection{Clique Averaging: Debiasing Gradients from Inter-Clique Edges}
% \label{section:clique-averaging}

While limiting the number of inter-clique connections reduces the
amount of messages traveling on the network, it also introduces a form of
bias.
Figure~\ref{fig:connected-cliques-bias} illustrates the problem on the
simple case of two cliques connected by one inter-clique edge (here,
between the green node of the left clique and the pink node of the right
clique). In this example, each node holds example of a single class. Let us
focus on node A. With weights computed as in \eqref{eq:metro},
node A's self-weight is $\frac{12}
{110}$, the weight between A and the green node connected to B is
$\frac{10}{110}$, and
all other neighbors of A have a weight of $\frac{11}{110}$. Therefore, the
gradient at A is biased towards its own class (pink) and against the green
class. A similar bias holds for all other nodes
without inter-clique edges with respect to their respective classes. For node
B, all its edge weights (including its self-weight) are equal to $\frac{1}
{11}$. However, the green class is represented twice (once as a clique
neighbor and once from the inter-clique edge), while all other classes are
represented only once. This biases the gradient toward the green class. The
combined effect of these two sources of bias is to increase the variance
of the local models across nodes.

\begin{figure}[t]
         \centering
         \includegraphics[width=0.28\textwidth]{figures/connected-cliques-bias}
\caption{\label{fig:connected-cliques-bias} Illustrating the bias induced by
inter-clique connections (see main text for details).}
\end{figure}

\paragraph{Clique Averaging} 
We address this problem by adding \emph{Clique
Averaging} to D-SGD
(\autoref{Algorithm:Clique-Unbiased-D-PSGD}), which essentially
decouples gradient averaging from model averaging. The idea is to use only the
gradients of neighbors within the same clique to compute the average gradient
so as to remove the bias due to inter-clique edges. In contrast, all
neighbors' models (including those in different cliques)
participate in model averaging as in the original version. Adding Clique Averaging
requires gradients to be sent separately from the model parameters: the number
of messages
exchanged between nodes is therefore twice their number of edges.

\begin{figure}[t]
  \hrule
   \begin{algorithmic}[1]
        \State \textbf{Require} initial model $\theta_i^{(0)}$, learning
        rate $\gamma$, mixing weights $W$, mini-batch size $m$, number of
        steps $K$
        \For{$k = 1,\ldots, K$}
          \State $S_i^{(k)} \gets \text{mini-batch of $m$ samples drawn
          from~} D_i$
          \State $g_i^{(k)} \gets \frac{1}{|\textit{Clique}(i)|}\sum_{j \in 
          \textit{Clique(i)}}  \nabla F(\theta_j^{(k-1)}; S_j^{(k)})$
          \State $\theta_i^{(k-\frac{1}{2})} \gets \theta_i^{(k-1)} - \gamma g_i^{(k)}$ 
          \State $\theta_i^{(k)} \gets \sum_{j \in N} W_{ji}^{(k)} \theta_j^{(k-\frac{1}{2})}$
        \EndFor
   \end{algorithmic}
   \hrule
   \caption{D-SGD with Clique Averaging (perspective of node $i$).}
   \label{Algorithm:Clique-Unbiased-D-PSGD}
\end{figure}


\paragraph{Implementing momentum with Clique Averaging}
% \label{section:momentum}
Efficiently training high capacity models usually requires additional
optimization techniques. In particular, momentum~\cite{pmlr-v28-sutskever13}
increases the magnitude of the components of the gradient that are shared
between several consecutive steps, and is critical for deep convolutional networks like
LeNet~\cite{lecun1998gradient,quagmire} to converge quickly. However, a direct
application of momentum in data heterogeneous settings can
actually be very detrimental and even fail to converge, as we will show in
 our experiments (Figure~\ref{fig:cifar10-c-avg-momentum} in
 Section~\ref{section:evaluation}).
% As illustrated in Figure~\ref{fig:d-cliques-cifar10-momentum-non-iid-effect}
% for the case of LeNet on CIFAR10 with 100 nodes, D-Cliques with momentum
% even fails to converge. Not using momentum actually gives a faster
% convergence, but there is a significant gap compared to the case of a single
% IID node with momentum.
Clique Averaging allows us to reduce the bias in the momentum by using the
clique-level average gradient $g_i^{(k)}$ of
\autoref{Algorithm:Clique-Unbiased-D-PSGD}:
\begin{equation}
v_i^{(k)} \leftarrow m v_i^{(k-1)} +  g_i^{(k)}.
\end{equation}
It then suffices to modify the original gradient step to apply momentum:
\begin{equation}
\theta_i^{(k-\frac{1}{2})} \leftarrow \theta_i^{(k-1)} - \gamma v_i^{(k)}.
\end{equation}

% \section{Scaling the Interclique Topology}
% \label{section:interclique-topologies}

